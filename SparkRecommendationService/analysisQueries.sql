--#avg price for each category
CREATE view avg_price_category
SELECT avg(CAST(price AS float)) as avg_price, category
FROM products
Group by category
ORDER BY avg_price DESC
LIMIT 5;


--# top 5 sold category

CREATE VIEW top_sold_category as
SELECT products.category,count(*) as transaction_count
FROM transaction LEFT JOIN products ON transaction.productid = cast(products.id AS BIGINT )
GROUP BY products.category
ORDER BY transaction_count DESC
LIMIT 5;

--# top 5 viewed category

CREATE VIEW top_viewed_category as
SELECT products.category,count(*) as view_count
FROM `view` 
LEFT JOIN products ON `view`.productid = cast(products.id AS BIGINT)
GROUP BY products.category
ORDER BY view_count DESC
LIMIT 5;


--# Avg money spent for each user

CREATE VIEW avg_spent_money_user_info as
SELECT *
From avg_spent_money_user
LEFT JOIN users on cast(users.id as BIGINT) = avg_spent_money_user.userid



CREATE VIEW avg_spent_money_user as
SELECT transaction.userid , avg(CAST(products.price AS float)) as avg_of_spent_money
From transaction 
LEFT JOIN products ON CAST(products.id AS BIGINT) = transaction.productid  
Group BY transaction.userid
ORDER BY avg_of_spent_money DESC
limit 5;


--sum of money spent by user 

CREATE VIEW total_spent_money_user_info as
SELECT *
From total_spent_money_user
LEFT JOIN users on cast(users.id as BIGINT) = total_spent_money_user.userid


CREATE VIEW total_spent_money_user as
SELECT transaction.userid , sum(CAST(products.price AS float)) as sum_of_spent_money
From transaction 
LEFT JOIN products ON CAST(products.id AS BIGINT) = transaction.productid  
Group BY transaction.userid
ORDER by sum_of_spent_money DESC
limit 5;


-- top 5 viewed product information

CREATE view top_viewed_product_info as
SELECT products.id, id_count, price, name 
from top_viewed_product
LEFT JOIN products on cast(top_viewed_product.productid as BIGINT) = cast(products.id as BIGINT);


-- top 5 sold product information

CREATE view  top_sold_product_info as
SELECT products.id, transaction_count , price, name, category
from top_sold_product
LEFT JOIN products on top_sold_product.productid = cast(products.id as BIGINT);


CREATE VIEW top_sold_product as
SELECT productid, count(productid) as transaction_count
FROM transaction 
-- WHERE day(time) in (select day(current_timestamp()))
GROUP by productid
ORDER BY transaction_count DESC
limit 5

--top 5 sold products for today

CREATE VIEW top_viewed_product as
SELECT productid, count(productid) as id_count
FROM transaction 
-- WHERE day(time) in (select day(current_timestamp()))
GROUP by productid
ORDER BY id_count DESC
limit 5

---------------------------------------------------------------------

--top 5 users for view


CREATE view top_users_view_info as
SELECT *
From top_users_view
LEFT JOIN users on cast(users.id as BIGINT) = top_users_view.userid

CREATE view top_users_view as
SELECT userid, count(userid) as userid_count
FROM `view` 
GROUP by userid
ORDER BY count(userid) DESC
limit 5


---------------------------------------------------------------------

--top 5 users for view

CREATE view top_users_view_daily as
SELECT userid, count(userid) as userid_count
FROM `view` 
-- WHERE day(time) in (select day(current_timestamp()))
GROUP by userid
ORDER BY count(userid) DESC
limit 5

-------------------------------------------------------------

--nested

-- SELECT * 
-- from users
-- WHERE users.id in(
SELECT transaction.userid
From transaction
LEFT JOIN products on transaction.productid = cast(products.id AS BIGINT)
GROUP BY transaction.userid
ORDER BY  count(*) DESC
-- )

--------------------------------------------------------------------
--
--top 5 users for login

CREATE view top_users_login_info as
SELECT *
From top_users_login
LEFT JOIN users on cast(users.id as BIGINT) = top_users_login.userid

CREATE VIEW top_users_login as
SELECT userid, count(userid) as userid_count
FROM `login` 
-- WHERE day(time) in (select day(current_timestamp()))
GROUP by userid
ORDER BY count(userid) DESC
limit 5


-----------------------------------------------------------------
--
--top 5 users for logout today

CREATE view top_users_logout_info as
SELECT *
From top_users_logout
LEFT JOIN users on cast(users.id as BIGINT) = top_users_logout.userid

CREATE VIEW top_users_logout as
SELECT userid, count(userid) as userid_count
FROM `logout` 
-- WHERE day(time) in (select day(current_timestamp()))
GROUP by userid
ORDER BY count(userid) DESC
limit 5


---------------------------------------------------------------

-- 
--top 5 users who made a transaction today

CREATE view top_users_transaction_info as
SELECT *
From top_users_transaction
LEFT JOIN users on cast(users.id as BIGINT) = top_users_transaction.userid


CREATE VIEW top_users_transaction as
SELECT userid, count(userid) as userid_count
FROM `transaction` 
-- WHERE day(time) in (select day(current_timestamp()))
GROUP by userid
ORDER BY count(userid) DESC
limit 5


-----------------------------------------------------------------

--top 5 users for logout today

CREATE view top_users_events_info as
SELECT *
From top_users_events
LEFT JOIN users on cast(users.id as BIGINT) = top_users_events.userid

CREATE VIEW top_users_events as
SELECT userid, count(userid) as num_of_usres
FROM `events` 
-- WHERE day(time) in (select day(current_timestamp()))
GROUP by userid
ORDER BY count(userid) DESC
limit 5

-----------------------------------------------------------------------------------
--top sold prpducts each category

SELECT *
from top_sold_product
LEFT JOIN products on top_sold_product.productid = cast(products.id as BIGINT)
WHERE products.category="Home Furnishing";

--------------------------------------------------------------------------------------
CREATE view avg_number_of_words as
SELECT avg(cast(noofwords as FLOAT)) as avg_number_of_words
from slack
GROUP BY user
ORDER BY avg_number_of_words DESC;


-------------------------------------------------------------------------------------------

--count of positive and negative messgages for each user

CREATE view count_of_pos_messages as
SELECT count(*) as number_of_psoitive_messages, user
from slack
where sentiment= "pos"
GROUP BY user


CREATE view count_of_neg_messages as
SELECT count(*) as number_of_negative_messages, user
from slack
where sentiment= "neg"
GROUP BY user


-------------------------
CREATE view sentiment_of_messages as
SELECT *
from count_of_neg_messages 
LEFT JOIN count_of_pos_messages on count_of_neg_messages.user = count_of_pos_messages.user