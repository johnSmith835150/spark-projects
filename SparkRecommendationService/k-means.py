from pyspark.sql import SparkSession
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.clustering import KMeans
from pyspark.ml.feature import StringIndexer
from pyspark.sql import *
from pyspark.sql.types import *
from pyspark.sql import Row

df = spark.read.load('users.csv', format ="csv" ,header='true', inferschema='true')

df = df.drop('balance' , 'nama' , 'email', 'password', 'photoUrl')
label_stringIdx = StringIndexer(inputCol = "country", outputCol = "country_index")
label_stringIdx_2 = StringIndexer(inputCol = "gender", outputCol = "gender_index")

model = label_stringIdx.fit(df)
indexed = model.transform(df)
model = label_stringIdx_2.fit(indexed)
df = model.transform(indexed)

vecAssembler = VectorAssembler(inputCols=["gender_index", "country_index", "age"], outputCol="features")
df = vecAssembler.transform(df)
kmeans = KMeans(k=4, seed=1)
model = kmeans.fit(df.select('features'))
transformed = model.transform(df)
transformed.show()
