from argparse import ArgumentParser
from flask import Flask, jsonify, request

from pyspark.sql import SparkSession
from pyspark.ml.evaluation import RegressionEvaluator
from pyspark.ml.recommendation import ALS, ALSModel
from pyspark.sql import Row
from pyspark.sql.functions import udf
from pyspark.mllib.recommendation import MatrixFactorizationModel


MODEL_PATH = "hdfs:///user/root/als2.model"
NUM_RECOMMENDED_ITEMS = 10


app = Flask(__name__)
spark = SparkSession.builder.master("yarn").appName('alsModel').getOrCreate()
model = ALSModel.load(MODEL_PATH)


def extractProductIds(items):
    return [item[0] for item in items]


def predict(sparkSession, model, id):
    userRecs = model.recommendForAllUsers(10)

    user = spark.createDataFrame([(id,)], ["userId"])
    specificRecs = model.recommendForUserSubset(user, NUM_RECOMMENDED_ITEMS)

    extractProductIdsUdf = udf(extractProductIds)
    specificRecs = specificRecs.withColumn("recommendations", extractProductIdsUdf("recommendations"))
    productIds = specificRecs.select('recommendations').collect()

    return eval(productIds[0][0])


@app.route('/reload', methods=['GET'])
def reloadModel():
	model = ALSModel.load(MODEL_PATH)
	return jsonify({'message': "done", 'errors': None}), 201


@app.route('/change-numitems', methods=['POST'])
def setNumRecommendedItems():
	values = request.get_json()
	
	required = ['numItems']
	if not all(k in values for k in required):
		return 'Missing values', 400

	NUM_RECOMMENDED_ITEMS = int(values['numItems'])

	return jsonify({'message': "done", 'errors': None}), 201


@app.route('/change-path', methods=['POST'])
def setModelPath():
	values = request.get_json()
	
	required = ['path']
	if not all(k in values for k in required):
		return 'Missing values', 400

	MODEL_PATH = values['path']
	model = ALSModel.load(MODEL_PATH)

	return jsonify({'message': "done", 'errors': None}), 201


@app.route('/recommend', methods=['GET'])
def recommend():
    id = request.args.get('userId')
    recommendation = predict(spark, model, id)

    return jsonify({'message': "done", 'errors': None, 'recommendation': recommendation}), 201


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument('-p', '--port', default=5000,
                        type=int, help='port to listen on')
    args = parser.parse_args()
    port = args.port

    app.run(host='0.0.0.0', port=port)
