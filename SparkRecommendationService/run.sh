#!/bin/bash

uage()
{
    echo "usage: run spark application [[[-f | --file python file to excute ] | [-h | --help]]"
}

id="spark_app"
master="yarn"
mode="client"
file=""
name="sparkApplication"
executorMemory="1G"
driverMemory="3G"
executorCores=2
numExecutors=6
stream=0
jars=""
args=""

while [ "$1" != "" ]; do
    case $1 in
        -i | --id )                 shift
                                    id=$1
                                    ;;
        -ma | --master )            shift
                                    master=$1
                                    ;;
        -mo | --mode )              shift
                                    mode=$1
                                    ;;
        -f | --file )               shift
                                    file=$1
                                    ;;
        -em | --executor-memory )   shift
                                    executorMemory=$1
                                    ;;
        -dm | --driver-memory )     shift
                                    driverMemory=$1
                                    ;;
        -ec | --executor-cores )    shift
                                    executorCores=$1
                                    ;;
        -ne | --num-executors )     shift
                                    numExecutors=$1
                                    ;;
        -n | --name )               shift
                                    name=$1
                                    ;;
        -s | --stream )             stream=1
                                    ;;
        -j | --jars )               shift
                                    jars=$1
                                    ;;
        -a | --args )               shift
                                    args=$1
                                    ;;
        -h | --help )               usage
                                    exit
                                    ;;
        * )                         usage
                                    exit 1
    esac
    shift
done

if [ "$file" = "" ]; then
	echo "Error: file name must be provided"
    exit
fi

if [[ ! -f $file ]]; then
    echo "$file is not a valid python file"
    exit 1
fi

case $file in
  /*) file=$file ;;
  *) file="${PWD}/${file}" ;;
esac

SPARK_STREAM_JAR_PATH=${PWD}/jars/spark-streaming-kafka-0-8-assembly_2.11-2.4.3.jar

if [ "$stream" == "1" ]; then
    jars="${jars},${SPARK_STREAM_JAR_PATH}"
fi

id="spark.app.id=${id}"

if [ "$master" == "yarn" ]; then
    spark-submit --master yarn --deploy-mode ${mode} --executor-memory ${executorMemory}  --driver-memory ${driverMemory} \
    --executor-cores ${executorCores} --num-executors ${numExecutors} --name ${name} --jars ${jars}  --conf ${id} ${file} ${args}
else
    spark-submit --master local --executor-memory ${executorMemory}  --driver-memory ${driverMemory} \
    --executor-cores ${executorCores} --num-executors ${numExecutors} --name ${name} --jars ${jars}  --conf ${id} ${file} ${args}
fi

