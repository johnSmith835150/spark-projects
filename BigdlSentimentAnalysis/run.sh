#!/bin/bash

uage()
{
    echo "usage: bigdl [[[-f | --file python file to excute ] | [-h | --help]]"
}

mode="client"
file=""
executorMemory="1g"
driverMemory="2g"
executorCores=2
numExecutors=6
stream=0
jars=""
args=""

while [ "$1" != "" ]; do
    case $1 in
        -m | --mode )               shift
                                    mode=$1
                                    ;;
        -f | --file )               shift
                                    file=$1
                                    ;;
        -em | --executor-memory )   shift
                                    executorMemory=$1
                                    ;;
        -dm | --driver-memory )     shift
                                    driverMemory=$1
                                    ;;
        -ec | --executor-cores )    shift
                                    executorCores=$1
                                    ;;
        -ne | --num-executors )     shift
                                    numExecutors=$1
                                    ;;
        -s | --stream )             stream=1
                                    ;;
        -j | --jars )               shift
                                    jars=$1
                                    ;;
        -a | --args )               shift
                                    args=$1
                                    ;;
        -h | --help )               usage
                                    exit
                                    ;;
        * )                         usage
                                    exit 1
    esac
    shift
done

if [ "$file" = "" ]; then
	echo "Error: file name must be provided"
    exit
fi

if [[ ! -f $file ]]; then
    echo "$file is not a valid python file"
    exit 1
fi

case $file in
  /*) file=$file ;;
  *) file="${PWD}/${file}" ;;
esac

BigDL_HOME=/opt/bigDl
SPARK_HOME="/opt/cloudera/parcels/CDH-6.2.0-1.cdh6.2.0.p0.967373/lib/spark"
PYTHON_API_PATH=${BigDL_HOME}/lib/bigdl-0.9.0-python-api.zip
BigDL_JAR_PATH=${BigDL_HOME}/lib/bigdl-SPARK_2.4-0.9.0-jar-with-dependencies.jar
PYTHONPATH=${PYTHON_API_PATH}:$PYTHONPATH
VENV_HOME=/opt/bigDl/bin
SPARK_STREAM_JAR_PATH=${PWD}/jars/spark-streaming-kafka-0-8-assembly_2.11-2.4.3.jar

if [ "$jars" == "" ]; then
    jars="${BigDL_JAR_PATH}"
else
    jars="${BigDL_JAR_PATH},${jars}"
fi

if [ "$stream" == "1" ]; then
    jars="${jars},${SPARK_STREAM_JAR_PATH}"
fi

PYSPARK_PYTHON=/opt/bigDl/bin/venv-compressed/venv/bin/python ${SPARK_HOME}/bin/spark-submit \
--conf spark.yarn.appMasterEnv.PYSPARK_PYTHON=/opt/bigDl/bin/venv-compressed/venv/bin/python \
--master yarn-client \
--executor-memory ${executorMemory} \
--driver-memory ${driverMemory} \
--executor-cores ${executorCores} \
--num-executors ${numExecutors} \
--properties-file ${BigDL_HOME}/conf/spark-bigdl.conf \
--jars ${jars} \
--py-files ${PYTHON_API_PATH} \
--archives ${VENV_HOME}/venv-compressed \
--conf spark.driver.extraClassPath=bigdl-SPARK_2.4-0.9.0-jar-with-dependencies.jar \
--conf spark.executor.extraClassPath=bigdl-SPARK_2.4-0.9.0-jar-with-dependencies.jar \
${file} ${args}
