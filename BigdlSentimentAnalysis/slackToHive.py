import json
from argparse import ArgumentParser

from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.sql.types import StructType, StructField, StringType
from pyspark.streaming.kafka import KafkaUtils
from pyspark.sql import Row, HiveContext
from pyspark.sql import SQLContext, DataFrame


def getHiveContextInstance(sparkContext):
    if ('hiveContextSingletonInstance' not in globals()):
        globals()['hiveContextSingletonInstance'] = HiveContext(sparkContext)
    return globals()['hiveContextSingletonInstance']


def process(rdd):
    if rdd.isEmpty():
        return

    context = getHiveContextInstance(rdd.context)

    parsed = rdd.map(lambda w: json.loads(w[1]))
    data = parsed.map(lambda e: Row(text=str(e["text"]), user=str(e["user"]), language=str(e["language"]),noOfWords=str(e["noOfWords"]), timestamp=str(
        e["timestamp"]), preprocessedText=e["preprocessedText"], sentiment=str(e["sentiment"])))

    df = context.createDataFrame(data)
    df.write.mode('append').format(
        "parquet").saveAsTable("slack")


topic = "slack-to-hive"
brokers = "192.168.23.140:9092,192.168.23.141:9092,192.168.23.142:9092"

sc = SparkContext(appName="PredictionToHiveTable")
ssc = StreamingContext(sc, 1)
sqlContext = SQLContext(sc)

kafkaParams = {"metadata.broker.list": brokers,
                "auto.offset.reset": "smallest"}
kafkaDStream = KafkaUtils.createDirectStream(ssc, [topic], kafkaParams)

kafkaDStream.foreachRDD(process)

ssc.start()
ssc.awaitTermination()
