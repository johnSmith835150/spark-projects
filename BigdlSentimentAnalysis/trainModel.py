from __future__ import print_function
import re
import json
import numpy as np
import pandas as pd

from collections import Counter, OrderedDict

from pyspark.sql import SparkSession, Row
from pyspark.sql.functions import *
from pyspark.sql.functions import udf
from pyspark.ml.feature import HashingTF, Tokenizer
from pyspark import SparkContext

from bigdl.nn.keras.layer import *
from bigdl.nn.keras.topology import Sequential
from bigdl.nn.criterion import *
from bigdl.optim.optimizer import *
from bigdl.util.common import *

from nltk.corpus import stopwords
from nltk.stem.porter import *

from keras.preprocessing.text import Tokenizer
from keras.preprocessing.text import text_to_word_sequence
from keras.preprocessing.sequence import pad_sequences

import time

def stem(x):
    stemmer = PorterStemmer()
    words = x.split()
    stemmedWords = [stemmer.stem(word) for word in words]
    return ' '.join(stemmedWords)


def remove_stop_words(x):
    stop_words = set(stopwords.words('english'))
    processed_words = []

    words = x.split()
    for word in words:
        if word not in stop_words:
            processed_words.append(word)

    return ' '.join(processed_words)


def remove_punctuations(x):
    punctuations = ["@", "[", "]", "(", ")", ",", ".", "/", "_", "-", ":", "?"]
    for punctuation in punctuations:
        x = x.replace(punctuation, "")

    return x


def remove_numbers(x):
    return ''.join([i for i in x if not i.isdigit()])


def remove_non_ascii(x):
    return ''.join([i if ord(i) < 128 else ' ' for i in x])


np.random.seed(51)
MAX_WORD_TO_USE = 100000
MAX_LEN = 80

data = pd.read_csv('datasets/AmazonReviews.csv')
train_Y = pd.get_dummies(data['label']).values

data['text'] = data['text'].apply(lambda x: remove_non_ascii(x))
data['text'] = data['text'].apply(lambda x: ' '.join(text_to_word_sequence(x)))
data['text'] = data['text'].apply(lambda x: remove_stop_words(x))
data['text'] = data['text'].apply(lambda x: remove_punctuations(x))
data['text'] = data['text'].apply(lambda x: remove_numbers(x))
data['text'] = data['text'].apply(lambda x: stem(x))


# Tokenize the sentences
tokenizer = Tokenizer(nb_words=MAX_WORD_TO_USE)
textList = [str(sentence) for sentence in list(data['text'])]
tokenizer.fit_on_texts(textList)

train_X = tokenizer.texts_to_sequences(textList)
train_X = pad_sequences(train_X, maxlen=MAX_LEN)


### TODO save wordDictionary to HDFS to use latter for prediction
words_dict = tokenizer.word_index


embed_dim = 128
lstm_out = 124
batch_size = 768
max_fatures = 100000

model = Sequential()
model.add(Embedding(max_fatures, embed_dim,input_shape = (train_X.shape[1],)))
model.add(SpatialDropout1D(0.4))
model.add(LSTM(lstm_out,return_sequences=True))
model.add(LSTM(64))
model.add(Dense(2,activation='softmax'))
model.compile(loss = 'categorical_crossentropy', optimizer='adam',metrics = ['accuracy'])

train_data = to_sample_rdd(train_X, train_Y)
model.fit(train_data, nb_epoch =10,distributed=True, batch_size = batch_size)

modelFile = "hdfs:///user/root/LSTMmodelBatch768_F.bigdl"
modelFileWeights = "hdfs:///user/root/LSTMmodelBatch768_F.bin"

model.saveModel(modelFile, modelFileWeights, True)
