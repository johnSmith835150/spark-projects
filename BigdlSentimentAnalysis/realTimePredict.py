from __future__ import print_function
import numpy as np
from collections import Counter
import re
import json

from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from pyspark.sql.functions import udf, monotonically_increasing_id
from pyspark.ml.feature import HashingTF, Tokenizer
from pyspark import SparkContext

from pyspark.streaming import StreamingContext
from pyspark.sql.types import StructType, StructField, StringType
from pyspark.streaming.kafka import KafkaUtils
from pyspark.sql import SQLContext, DataFrame, Row
from pyspark.sql.types import StringType

from bigdl.nn.layer import *
from bigdl.optim.optimizer import *
from bigdl.nn.keras.layer import *
from bigdl.nn.keras.topology import Sequential
from bigdl.nn.criterion import *
from bigdl.util.common import *
from nltk.stem.snowball import SnowballStemmer
from nltk.corpus import stopwords
from kafka import KafkaProducer

producer = KafkaProducer(bootstrap_servers=['192.168.23.140:9092', '192.168.23.141:9092',
                                            '192.168.23.142:9092'], value_serializer=lambda x: json.dumps(x).encode('utf-8'))

sc = SparkContext.getOrCreate(conf=create_spark_conf().setMaster(
    "yarn").set("spark.driver.memory", "10g"))
ssc = StreamingContext(sc, 1)
sqlContext = SQLContext(sc)

model = Model.loadModel("hdfs:///user/root/LSTMmodel.bigdl",
                        "hdfs:///user/root/LSTMmodel.bin")


def predictSentiment(rdd):
    if rdd.isEmpty():
        return

    parsed = rdd.map(lambda w: json.loads(w[1]))
    data = parsed.map(lambda e: Row(text=str(e["text"]), query=str(e["query"]), language=str(
        e["language"]), timestamp=str(e["timestamp"]), preprocessedText=e["preprocessedText"]))

    df = sqlContext.createDataFrame(data)
    df.printSchema()

    features = df.select('preprocessedText').collect()

    print("An RDD")
    X = []
    for feature in features:
        X.append(feature[0])

    X = np.array(X)

    print(X)
    print(type(X))

    result = model.predict(X, distributed=True)
    results = result.collect()
    predictionsLabels = ["pos", "neg"]
    predictions = []
    for i in range(len(results)):
        predictions.append(predictionsLabels[np.argmax(results[i])])

    predictionsDF = sqlContext.createDataFrame(predictions, StringType())
    predictionsDF = predictionsDF.withColumnRenamed("value", "sentiment")

    df11 = df.select("*").withColumn("columnindex",
                                     monotonically_increasing_id())
    df22 = predictionsDF.select(
        "*").withColumn("columnindex", monotonically_increasing_id())
    joined = df11.join(df22, df11.columnindex ==
                       df22.columnindex, 'inner').drop(df22.columnindex)
    joined = joined.drop(joined.columnindex)

    joined.printSchema()
    list_elements = joined.collect()
    messages = []
    for element in list_elements:
        message = {
            'language': str(element[0]),
            'preprocessedText': eval(str(element[1])),
            'query': str(element[2]),
            'text': str(element[3]),
            'timestamp': str(element[4]),
            'sentiment': str(element[5])
        }
        messages.append(message)

    for message in messages:
        producer.send("sentiment-analysis-prediction", value=message)


init_engine()
show_bigdl_info_logs()

topic = "sentiment-output"
brokers = "192.168.23.140:9092,192.168.23.141:9092,192.168.23.142:9092"

kafkaParams = {"metadata.broker.list": brokers,
               "auto.offset.reset": "smallest"}
kafkaDStream = KafkaUtils.createDirectStream(ssc, [topic], kafkaParams)


kafkaDStream.foreachRDD(predictSentiment)


ssc.start()
ssc.awaitTermination()
